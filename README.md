# Sequelize

postgres database migration

#### To initialize all databases
Your environment must already have database created if not, you have to create database and set config in ```config/config.json```

Then, create tables

```
sequelize db:migrate --env local --config config/config.json --migrations-path migrations/initialize
```

and then seed all data

```
sequelize db:seed:all --env local --config config/config.json --seeders-path seeders/initialize
```
