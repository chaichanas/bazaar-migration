'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface
        .rawSelect(
          'roles',
          {
            where: {
              name: 'admin',
            },
          },
          ['id'],
        )
        .then((id) => {
          if (!id) {
            // do bulkInsert stuff.
            queryInterface.bulkInsert('roles', [{ id: 1, name: 'admin', description: 'ผู้ดูแลระบบ', created_at: new Date(), updated_at: new Date() }]);
          }
        });
      await queryInterface
        .rawSelect(
          'roles',
          {
            where: {
              name: 'vendor',
            },
          },
          ['id'],
        )
        .then((id) => {
          if (!id) {
            // do bulkInsert stuff.
            queryInterface.bulkInsert('roles', [{ id: 2, name: 'vendor', description: 'ผู้ใช้งานที่ต้องการขายสินค้า', created_at: new Date(), updated_at: new Date() }]);
          }
        });
      await queryInterface
        .rawSelect(
          'roles',
          {
            where: {
              name: 'member',
            },
          },
          ['id'],
        )
        .then((id) => {
          if (!id) {
            // do bulkInsert stuff.
            queryInterface.bulkInsert('roles', [{ id: 3, name: 'member', description: 'ผู้ใช้งานปกติ ซื้อสินค้าได้เท่านั้น', created_at: new Date(), updated_at: new Date() }]);
          }
        });
      await queryInterface
        .rawSelect(
          'users',
          {
            where: {
              username: 'admin',
            },
          },
          ['id'],
        )
        .then(async (id) => {
          if (!id) {
            // do bulkInsert stuff.
            await queryInterface.bulkInsert('users', [{ 
              username: 'admin', 
              password: 'admin', 
              firstname: 'admin', 
              lastname: 'admin', 
              email: 'admin@example.com', 
              phone_number: '0853997206', 
              address: '159/77 m.10', 
              date_of_birth: '1998-06-22', 
              image_url: '', 
              status: 'active', 
              created_at: new Date(), 
              updated_at: new Date() 
            }]);
          }
        });
        await createAdmin(queryInterface);
        await createAdminShop(queryInterface);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

async function createAdmin(queryInterface) {
  await queryInterface
      .rawSelect(
        'users',
         {
          where: { username: 'admin' },
        },
        ['id'],
      )
      .then(async (id) => {
        if (!id) {
          await queryInterface.bulkInsert('users', [{ 
            username: 'admin', 
            password: 'admin', 
            firstname: 'admin', 
            lastname: 'admin', 
            email: 'admin@example.com', 
            phone_number: '0853997206', 
            address: '159/77 m.10', 
            date_of_birth: '1998-06-22', 
            image_url: '', 
            status: 'active', 
            created_at: new Date(), 
            updated_at: new Date() 
          }]);
        } else {
          await queryInterface
            .rawSelect(
              'user_roles',
              {
                where: { user_id: id },
              },
              ['id'],
            )
            .then(async (user_role_id) => {
              if (!user_role_id) {
                await queryInterface.bulkInsert('user_roles', [{ 
                  user_id: id,
                  role_id: 1,
                  created_at: new Date(), 
                  updated_at: new Date() 
                }]);
              }
            });
        }
      });
}

async function createAdminShop(queryInterface) {
  await queryInterface
      .rawSelect(
        'shops',
         {
          where: { name: 'admin_shop' },
        },
        ['id'],
      )
      .then(async (id) => {
        if (!id) {
          await queryInterface.bulkInsert('shops', [{ 
            name: 'admin_shop',
            description: 'admin shop',
            is_open: true,
            created_at: new Date(), 
            updated_at: new Date() 
          }]);
        } else {
          await queryInterface
            .rawSelect(
              'user_shops',
              {
                where: { shop_id: id },
              },
              ['id'],
            )
            .then(async (user_shop_id) => {
              if (!user_shop_id) {
                await queryInterface.bulkInsert('user_shops', [{ 
                  shop_id: id,
                  user_id: 1,
                  created_at: new Date(), 
                  updated_at: new Date() 
                }]);
              }
            });
        }
      });
}