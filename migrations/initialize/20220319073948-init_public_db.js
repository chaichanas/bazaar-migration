'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        username: { type: Sequelize.STRING, unique: true },
        password: { type: Sequelize.TEXT },
        firstname: { type: Sequelize.STRING },
        lastname: { type: Sequelize.STRING },
        email: { type: Sequelize.STRING },
        phone_number: { type: Sequelize.STRING },
        address: { type: Sequelize.TEXT },
        date_of_birth: { type: Sequelize.DATE },
        image_url: { type: Sequelize.TEXT },
        status: {
          type: Sequelize.ENUM({
            name: 'enum_status_id',
            values: ['active', 'inactive'],
          }),
        },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('roles', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        name: { type: Sequelize.STRING, unique: true },
        description: { type: Sequelize.TEXT },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('user_roles', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'users',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        role_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'roles',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('shops', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        name: { type: Sequelize.STRING, unique: true },
        description: { type: Sequelize.TEXT },
        address: { type: Sequelize.TEXT },
        email: { type: Sequelize.STRING },
        phone_number: { type: Sequelize.STRING },
        is_open: { type: Sequelize.BOOLEAN },
        image_url: { type: Sequelize.TEXT },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('user_shops', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'users',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        shop_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'shops',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('products', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        name: { type: Sequelize.STRING },
        description: { type: Sequelize.TEXT },
        price: { type: Sequelize.DOUBLE },
        is_soldout: { type: Sequelize.BOOLEAN },
        image_url: { type: Sequelize.TEXT },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('shop_products', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        shop_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'shops',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        product_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'products',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('orders', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'users',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        status: {
          type: Sequelize.ENUM({
            name: 'enum_status_id',
            values: ['preparing', 'shipping', 'shipped', 'success', 'fail'],
          }),
        },
        is_paid: { type: Sequelize.BOOLEAN },
        price: { type: Sequelize.DOUBLE },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
    await queryInterface.createTable('transactions', {
        id: {
          type: Sequelize.BIGINT,
          autoIncrement: true,
          primaryKey: true,
        },
        order_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'orders',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        product_id: {
          type: Sequelize.BIGINT,
          references: {
            model: 'products',
            key: 'id',
          },
          onDelete: 'cascade',
        },
        price: { type: Sequelize.DOUBLE },
        amount: { type: Sequelize.INTEGER },
        created_at: { type: Sequelize.DATE },
        updated_at: { type: Sequelize.DATE },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('transactions');
    await queryInterface.dropTable('orders');
    await queryInterface.dropTable('shop_products');
    await queryInterface.dropTable('user_shops');
    await queryInterface.dropTable('user_roles');
    await queryInterface.dropTable('products');
    await queryInterface.dropTable('shops');
    await queryInterface.dropTable('roles');
    await queryInterface.dropTable('users');
  }
};
